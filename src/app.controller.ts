import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Post,
  Req,
  Response,
  ValidationPipe,
} from '@nestjs/common';
import { AppService } from './app.service';
import CreateUserDTO from './schema/CreateUserDTO';
import UserEntity from './db/entities/user.entity';
import AddFavCity from './schema/AddFavCity';
import DeleteFavCity from './schema/DeleteFavCity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/users')
  async findAllUsers(): Promise<UserEntity[]> {
    return await this.appService.findAllUsers();
  }

  @Post('/signup')
  async createUser(
    @Body(new ValidationPipe()) createUserDTO: CreateUserDTO,
    @Req() req,
    @Response() res,
  ): Promise<any> {
    console.log(createUserDTO);
    const msg = await this.appService.createUser(createUserDTO);
    if (msg.code === HttpStatus.BAD_REQUEST) {
      res.status(HttpStatus.BAD_REQUEST).json(msg);
    } else {
      res.status(HttpStatus.OK).json(msg);
    }
  }

  @Post('/user/add')
  async addFavCityToUser(
    @Body(new ValidationPipe()) addFavCity: AddFavCity,
    @Req() req,
    @Response() res,
  ): Promise<any> {
    console.log(addFavCity);
    const msg = await this.appService.addFavCityToUser(addFavCity);
    if (msg.code === HttpStatus.BAD_REQUEST) {
      res.status(HttpStatus.BAD_REQUEST).json(msg);
    } else {
      res.status(HttpStatus.OK).json(msg);
    }
  }

  @Delete('/user/city/remove')
  async deleteCityOfUser(
    @Body(new ValidationPipe()) deleteFavCity: DeleteFavCity,
  ) {
    return await this.appService.deleteFavCityOfUser(deleteFavCity);
  }

  @Get('/getUserByEmail')
  async findUserByEmail(@Body('email') email: string): Promise<UserEntity> {
    return await this.appService.findUserByEmail(email);
  }

  @Get('/getUserByContact')
  async findUserByContact(
    @Body('contact') contact: string,
  ): Promise<UserEntity> {
    return await this.appService.findUserByContact(contact);
  }

  @Get('/mail')
  async sendMail() {
    return await this.appService.sendUserConfirmation();
  }
}
