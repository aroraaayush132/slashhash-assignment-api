import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import FavCityEntity from './fav-city.entity';
@Entity()
class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ unique: true })
  email: string;

  @Column({ unique: true })
  contact: string;

  @Column()
  gender: string;

  @OneToMany((type) => FavCityEntity, (favCityEntity) => favCityEntity.user, {
    eager: true,
  })
  favCities: FavCityEntity[];

  public static async getAllUsers(): Promise<UserEntity[]> {
    return await UserEntity.find();
  }

  public static async findById(id: number): Promise<UserEntity> {
    return await UserEntity.findOne({
      select: ['name', 'email', 'contact'],
      where: { id: id },
    });
  }

  public static async getUserByName(name: string): Promise<UserEntity[]> {
    return await UserEntity.find({
      select: ['name', 'email', 'contact'],
      where: { name },
    });
  }

  public static async getUserByEmail(email: string): Promise<UserEntity> {
    return await UserEntity.findOne({
      where: { email },
    });
  }

  public static async getUserByContact(contact: string): Promise<UserEntity> {
    return await UserEntity.findOne({
      select: ['contact'],
      where: { contact },
    });
  }

  public static async removeUser(email: string): Promise<UserEntity> {
    return await UserEntity.remove(
      await UserEntity.findOne({ where: { email } }),
    );
  }
}

export default UserEntity;
