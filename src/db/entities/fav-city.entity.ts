import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import UserEntity from './user.entity';
import DeleteFavCity from '../../schema/DeleteFavCity';

@Entity()
class FavCityEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  city: string;

  @ManyToOne((type) => UserEntity, (user) => user.favCities)
  user: UserEntity;

  public static async findById(id: number): Promise<FavCityEntity> {
    return await FavCityEntity.findOne({
      select: ['city'],
      where: { id },
    });
  }

  public static async getAllFavCities(): Promise<FavCityEntity[]> {
    return await FavCityEntity.find({
      select: ['city'],
    });
  }

  public static async removeCity(
    deleteFavCity: DeleteFavCity,
  ): Promise<FavCityEntity> {
    return await FavCityEntity.remove(
      await FavCityEntity.findOne({
        where: { city: deleteFavCity.city, user: deleteFavCity.id },
      }),
    );
  }
}

export default FavCityEntity;
