import { IsEmail, IsNotEmpty } from 'class-validator';

class CreateUserDTO {
  @IsNotEmpty()
  name: string;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  contact: string;

  @IsNotEmpty()
  gender: string;
}

export default CreateUserDTO;
