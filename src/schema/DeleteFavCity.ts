import { IsNotEmpty, IsNumber } from 'class-validator';

class DeleteFavCity {
  @IsNumber()
  id: number;

  @IsNotEmpty()
  city: string;
}

export default DeleteFavCity;
