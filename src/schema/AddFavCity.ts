import { IsNotEmpty, IsNumber } from 'class-validator';

class AddFavCity {
  @IsNumber()
  id: number;

  @IsNotEmpty()
  city: string;
}

export default AddFavCity;
