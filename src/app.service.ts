import { HttpStatus, Injectable } from '@nestjs/common';
import UserEntity from './db/entities/user.entity';
import CreateUserDTO from './schema/CreateUserDTO';
import AddFavCity from './schema/AddFavCity';
import FavCityEntity from './db/entities/fav-city.entity';
import DeleteFavCity from './schema/DeleteFavCity';
import { sendEmail } from './utils/sendEmail';

@Injectable()
export class AppService {

  getHello(): string {
    return 'Hello World!';
  }

  async createUser(createUserDTO: CreateUserDTO) {
    const { email } = createUserDTO;

    const user: UserEntity = await UserEntity.getUserByEmail(email);
    if (!user) {
      const newUser: UserEntity = await UserEntity.create(createUserDTO);
      const data = await UserEntity.save(newUser);
      return { message: 'User Signed Up Successfully', code: HttpStatus.OK };
    } else {
      return {
        message: 'Email already in use',
        code: HttpStatus.BAD_REQUEST,
      };
    }
  }

  async addFavCityToUser(addFavCity: AddFavCity) {
    const { id, city } = addFavCity;

    const user: UserEntity = await UserEntity.findOne(id);
    if (user) {
      const favCityEntity: FavCityEntity = await FavCityEntity.create();
      favCityEntity.city = city;
      favCityEntity.user = user;
      const data = await FavCityEntity.save(favCityEntity);
      return { message: 'Add Fav City Successfully', code: HttpStatus.OK };
    } else {
      return {
        message: 'User does not exists!!',
        code: HttpStatus.BAD_REQUEST,
      };
    }
  }

  async findAllUsers(): Promise<UserEntity[]> {
    return await UserEntity.getAllUsers();
  }

  async findUserByEmail(email: string): Promise<UserEntity> {
    return await UserEntity.getUserByEmail(email);
  }

  async findUserByContact(contact: string): Promise<UserEntity> {
    return await UserEntity.getUserByContact(contact);
  }

  async deleteFavCityOfUser(deleteFavCity: DeleteFavCity) {
    const user = await UserEntity.findOne(deleteFavCity.id);
    try {
      const favCities = await FavCityEntity.removeCity(deleteFavCity);
      return {
        message: `Removed City ${deleteFavCity.city} for user ${user.email}`,
        code: HttpStatus.OK,
      };
    } catch (e) {
      return {
        message: `City ${deleteFavCity.city} does not exists for user ${user.email}`,
        code: HttpStatus.BAD_REQUEST,
      };
    }
  }

  async sendUserConfirmation() {
    await sendEmail('manojarora894@gmail.com');
    return null;
  }
}
